data "aws_vpc" "default" {
  default = true
}

variable "tf53_traffic_rules" {
  type = list(object({
    from_port = number
    to_port = number
    protocol = string
    cidr_blocks = list(string)
  }))
  default = [
    {
      from_port = 22
      to_port = 22
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    },
    {
      from_port = 80 
      to_port = 80
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    },
    {
      from_port = 8001
      to_port = 8001
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    },
    {
      from_port = 8802
      to_port = 8802
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  ]
}

variable "ebs_volumes" {
  description = "Disks for attachment"
  type        = map(object({
    size = number
    device_name = string
    tag = map(string) 
  }))
  default     = {
    disk1 = {
      size = 10
      device_name = "sdb"
      tag = {
        Name = "tf53_disk1"
      }
    }
    disk2 = {
      size = 8
      device_name = "sdc"
      tag = {
        Name = "tf53_disk2"
      }
    }
  }
}

resource "aws_security_group" "tf53_security_group" {
  name = "tf53_sg"
  description = "Allow 80 8001 8802"

  dynamic "ingress" {
    for_each = var.tf53_traffic_rules
    content {
     from_port = ingress.value.from_port
     to_port = ingress.value.to_port
     protocol = ingress.value.protocol
     cidr_blocks = ingress.value.cidr_blocks
    }
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "tf53_sg"
  }
}

resource "aws_ebs_volume" "ebs_volumes" {
  for_each = var.ebs_volumes

  availability_zone = "eu-central-1a"
  size              = each.value.size
  tags              = each.value.tag
}

resource "aws_instance" "app_server" {
  ami           = "ami-00ad2436e75246bba"
  instance_type = "t2.micro"
  availability_zone = "eu-central-1a"
  iam_instance_profile = "DemoRoleforEC2"
  key_name = "tutorial_key"
  security_groups = ["tf53_sg"]
  root_block_device {
    volume_size = "20"
  }
  associate_public_ip_address = false

  tags = {
    Name = "eugesterh"
  }
  
  provisioner "local-exec" {
    command = "echo '${self.public_dns} ${self.public_ip}' >> hosts.list"
  }
  
  connection {
    type = "ssh"
    user = "ec2-user"
    private_key = "${file(var.private_key_path)}"
    timeout = "4m"
    host = self.public_ip
  }
  provisioner "remote-exec" {
    inline = [
      "sudo yum install -y docker",
      "sudo systemctl start docker",
      "sudo usermod -a -G docker ec2-user",
      "sudo mkdir -p /usr/share/nginx/html",
      "sudo chmod -R g+rw /usr/share/nginx/html",
      "sudo chgrp -R ec2-user /usr/share/nginx/html",
      "sudo echo 'Juneway ${self.tags.Name} ${self.public_ip}' > /usr/share/nginx/html/index.html",
      "sudo docker run -d -p 80:80 -v /usr/share/nginx/html:/usr/share/nginx/html nginx:1.24.0"
    ]  
  }
}

resource "aws_eip" "static_ip" {
  instance = aws_instance.app_server.id
  vpc      = true 

  tags = {
    Name = "app_server_eip"
  }
}

resource "cloudflare_record" "eugesterh_record" {
  zone_id = var.zone_id
  name    = "eugesterh.juneway.pro"
  value   = aws_instance.app_server.public_ip
  type    = "A"
  ttl     = 3600
}

resource "aws_volume_attachment" "volume_attachments" {
  for_each = aws_ebs_volume.ebs_volumes
  
  device_name = "/dev/${var.ebs_volumes[each.key].device_name}"
  volume_id   = aws_ebs_volume.ebs_volumes[each.key].id
  instance_id = aws_instance.app_server.id
}
