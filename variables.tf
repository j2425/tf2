variable "api_key" {
  description = "Cloudflare API key"
}

variable "email" {
  description = "Email associated with Cloudflare account"
}

variable "private_key_path" {
    description = "Path to the ssh key file for ec2 access"
}

variable "zone_id" {
    description = "Zone ID for Cloudflare zone"
}

variable "account_id" {
    description = "Account ID for Cloudflare account"
}