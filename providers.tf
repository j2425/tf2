terraform {
  required_providers {
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  region  = "eu-central-1"
}

provider "cloudflare" {
  api_key = var.api_key
  email = var.email

}